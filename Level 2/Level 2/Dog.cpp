#include "stdafx.h"

#include "Dog.h"


int Dog::getCompetitions()
{
	return this->competitions;
}

void Dog::setCompetitions(int c)
{
	competitions = c;
}

Dog::Dog()
{
}

Dog::Dog(string n, int a, int c)
{
	name = n;
	age = a;
	competitions = c;
}


Dog::~Dog()
{
}

