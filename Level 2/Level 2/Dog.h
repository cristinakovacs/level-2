#pragma once
#include "Animal.h"
class Dog :
	public Animal
{
public:
	int competitions;
public:
	int getCompetitions();
	void setCompetitions(int c);
	Dog();
	Dog(string n, int a, int c);
	~Dog();
};

