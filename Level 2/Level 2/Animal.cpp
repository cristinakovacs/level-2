#include "stdafx.h"
#include "Animal.h"


int Animal::getAge()
{
	return this->age;
}

string Animal::getName()
{
	return string();
}

void Animal::setAge(int a)
{
	age = a;
}

void Animal::setName(string n)
{
	name = n;
}

Animal::Animal()
{
}

Animal::Animal(string n, int a)
{
	name = n;
	age = a;
	cout << "Animal constructor";
}


Animal::~Animal()
{
	cout << "Animal destructor";
}

