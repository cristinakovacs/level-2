#pragma once
#include<iostream>
#include<string.h>
using namespace std;
class Animal
{
public:
	string name;
	int age;

public:
	int getAge();
	string getName();
	void setAge(int a);
	void setName(string n);
	Animal();
	Animal(string n, int a);
	~Animal();
};

