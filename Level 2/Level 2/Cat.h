#pragma once
#include "Animal.h"
class Cat :
	public Animal
{
private:
	string color;
public:
	string getColor();
	void setColor(string c);
	Cat();
	Cat(string name, int age, string color);
	~Cat();
};

